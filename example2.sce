mode(-1);

// define auxiliary function that plots normalized average polyphase responses
function plot_avg_polyphase(fltr,style)
  theta = -0.5:0.001:0.5;
  vals = zeros(theta);
  for k=1:length(theta) do
    z0 = exp(2*%pi*%i*theta(k));
    vals(k) = sum(abs(horner(fltr,z0)));
  end
  plot(theta,vals/max(vals),style);
endfunction

// define and plot channel
z = poly(0,"z");
h = 1 - 0.3/z + 0.5/z^2 - 0.4/z^3 + 0.1/z^4 - 0.02/z^5 + 0.3/z^6 -0.1/z^7;
plot_avg_polyphase(h,'--b');

// define the channels polyphase components
h0 = 1 + 0.1/z;
h1 = -0.3 - 0.02/z;
h2 = 0.5 + 0.3/z;
h3 = -0.4 - 0.1/z;

// define blocked channel transfer function
Htilde = [h0 h3/z h2/z h1/z ; h1 h0 h3/z h2/z ; h2 h1 h0 h3/z ; h3 h2 h1 h0];

// define precoding bank
ftilde = [eye(3,3) ; zeros(1,3)];
Omega = horner(sfact(numer(horner(Htilde*ftilde,1/z)'*Htilde*ftilde*z^2)),1/z)';
f = ftilde*inv(Omega);
plot_avg_polyphase(horner(f,z^3)*[1 ; 1/z ; 1/z^2],'-.b');

// compute joint polyphase matrix of precoder and channel
H = tf2ss(Htilde*f);
H.dt = "d";

// compute equalization banks for L=0,1,2 and plot average polyphase responses
Ls = [0,1,2,5];					// delays
styles = ['-r','-g','-c','-m'];			// plot styles
for j=1:length(Ls) do
  INVSYS = h2invsyslin(H,struct("delay",Ls(j)));// compute optimal Ls(j)-delay inverse
  printf("\n||G|| for delay L=%i: %.4f\n",Ls(j),dh2norm(INVSYS));
  G = ss2tf(INVSYS);				// convert into rational matrix
  g = [1/z^2 1/z 1]*horner(G,z^3);		// compute synthesis filter
						// from polyphase matrix
  plot_avg_polyphase(g,styles(j));
end

// compute equalization bank for L=infinity and plot average polyphase response
[R0c,R0ac] = parapinv(H);			// compute causal and anti-causal part
						// of the para-pseudoinverse R0
						// (opt. inverse of H for L=infinity)
printf("\n||G|| for delay L=infinity: %.4f\n",sqrt(dh2norm(R0c)^2+dh2norm(R0ac)^2));
G = ss2tf(R0c) + horner(ss2tf(R0ac),1/z);	// transfer function of R0
g = [1/z^2 1/z 1]*horner(G,z^3);		// compute synthesis filters
						// from polyphase matrix
plot_avg_polyphase(g,'-k');

// annotate plots
xtitle("","Frequency f []","");
a = get("current_axes");
a.font_style = 2;
a.font_size = 4;
a.title.font_style = 2;
a.title.font_size = 4;
a.x_label.font_style = 2;
a.x_label.font_size = 4;
a.y_label.font_style = 2;
a.y_label.font_size = 4;