mode(-1);

// save & redefine number format
v = format();
format("v",10);

// define the system to be inverted
A = 0.5;
B = 1;
C = [-0.25 ; -0.5 ; -0.75];
D = [-0.5 ; -1 ; -1.5];
SYSH = syslin("d",A,B,C,D);
H = ss2tf(SYSH);

// define the weight
AW = 0;
BW = 1;
CW = [1 ; 0 ; 0];
DW = [0 ; 0.1 ; 0.1];
SYSW = syslin("d",AW,BW,CW,DW);
W = ss2tf(SYSW);

// compute Dplus and Dperp
Dplus = pinv(D);
[U,S,V] = svd(eye(3,3) - D*Dplus);
Dperp = U(:,1:2);

// compute A0 and B0
TMP1 = A - B*Dplus*C; 
TMP2 = -Dperp'*C;
Bperp = cplxstabil(TMP1',TMP2',"d")';
A0 = TMP1 + Bperp*TMP2;
B0 = -B*Dplus - Bperp*Dperp';

// compute G and Gperp
G = syslin("d",A0,B0,Dplus*C,Dplus);
Gperp = syslin("d",A0,B0,Dperp'*C,Dperp');

// compute A0W, B0W, and C0W
A0W = [AW 0 ; B0*CW A0];
B0W = [BW ; B0*DW];
C0W = [CW C];

// solve the Riccati equation (we add a small perturbation to ensure solvability)
[Y11,L1] = dare(A0W',C0W'*Dperp,Dperp'*DW*DW'*Dperp,B0W*DW'*Dperp, ...
  B0W*B0W'+1e-10*eye(2,2));

// compute y_12^(1)
y121 = (A0W*Y11*C0W' + B0W*DW' + L1'*Dperp'*(DW*DW'+C0W*Y11*C0W'))*Dplus';

// compute P, Pplus, L2, and L0
P = Dperp'*(DW*DW' + C0W*Y11*C0W')*Dperp;
Pplus = pinv(P);
L2 = -Pplus*Dperp'*[(C0W*Y11*C0W'+DW*DW')*Dplus'];
L0 = -Pplus*Dperp'*C0W*y121;

// compute a basis for the subspace of P^0.5 (we drop the small singular value of P)
[U,S1,V] = svd(P);
P012 = U(:,2:$)';

// compute C(z)=[C11(z);C12(z)]
R = [L1 L2]'*Dperp';
S = L0'*Dperp';
Ahat = [A0 0 ; Dplus*C 0];
Bhat = [B0 ; Dplus];
AhatW = [AW 0 0 ; Bhat*CW Ahat];
SYSC = syslin("d",AhatW+[R*C0W zeros(3,1)],[L1 L2]', ...
  [0 0 1 ; Dperp'*C0W zeros(2,1)], [0 0 ; 1 0 ; 0 1]);
    
// show intermediate results
disp(H,"H(z):");
disp(W,"W(z):");
disp(Dperp, "Dperp:");
disp(A0W,"A0 (bold):");
disp(B0W,"B0 (bold):");
disp(C0W,"C0 (bold):");
disp(Y11,"Y11:");
disp(L1,"L1:");
disp(Y11,"y121:");
disp(P,"P:");
disp(L2,"L2:");
disp(L0,"L0:");
disp(P012,"P0^0.5:");
disp(SYSC,"SYSC:");

// we check some transfer functions: first one is C(z), the next two 
// should be zero, last one should be 1/z
C = ss2tf(SYSC);
z = poly(0,"z");
disp(clean(C),"C(z):");
disp(clean(ss2tf((G*tf2ss(eye(3,3)/z)+(SYSC(1,:)+L0'*SYSC(2:3,:))*Gperp)*SYSW)), ...
  "(G(z)/z+(C11(z)+L0''*C12(z))*Gperp(z))*W(z):");
disp(clean(ss2tf(P012*SYSC(2:3,:)*Gperp*SYSW)), ...
  "P012*C21(z)*Gperp(z))*W(z):");
disp(clean(ss2tf((G*tf2ss(eye(3,3)/z) + (SYSC(1,:) ...
  + (rand(1,1,"normal")*P012+L0')*SYSC(2:3,:))*Gperp)*SYSW)), ...
  "(G(z)/z+(C11(z)(K*P012+L0'')*C21(z))*Gperp)*W(z) for some random K:");
disp(clean(ss2tf((G*tf2ss(eye(3,3)/z) + (SYSC(1,:) ...
  +(rand(1,1,"normal")*P012+L0')*SYSC(2:3,:))*Gperp)*SYSH)), ...
  "(G(z)/z+(C11(z)(K*P012+L0'')*C21(z))*Gperp)*H(z) for some random K:");

// restore old number format
if v(1)==0 then
  format("e",v(2));
else
  format("v",v(2));
end