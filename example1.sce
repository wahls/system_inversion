mode(-1);

// define auxiliary function that plots normalized average polyphase responses
function plot_avg_polyphase(fltr,style)
  theta = -0.5:0.001:0.5;
  vals = zeros(theta);
  for k=1:length(theta) do
    z0 = exp(2*%pi*%i*theta(k));
    vals(k) = sum(abs(horner(fltr,z0)));
  end
  plot(theta,vals/max(vals),style);
endfunction

// define analysis filters
z = poly(0,"z");
h0 = (0.4208*z+0.4208)/(z-0.1584);
h1 = (0.2452*z^2-0.2452)/(z^2+0.5095);
h2 = horner(h0,-z);
h = [h0 ; h1 ; h2];
plot_avg_polyphase(h,'--b');

// compute and show polyphase matrix of the analysis filter bank
h = tf2ss([h0 ; h1 ; h2]);
[A,B,C,D] = abcd(h);
H = syslin("d",A^2,[A*B A^2*B],C,[D C*B]);
H.dt = "d";
disp("H(z):");
disp(clean(ss2tf(H)));

// compute synthesis banks for L=0,1,2 and plot average polyphase responses
Ls = [0,1,2];					// delays
styles = ['-r','-g','-m'];			// plot styles
for j=1:length(Ls) do
  INVSYS = h2invsyslin(H,struct("delay",Ls(j)));// compute optimal Ls(j)-delay inverse
  printf("\n||G|| for delay L=%i: %.4f\n",Ls(j),dh2norm(INVSYS));
  G = ss2tf(INVSYS);				// convert into rational matrix
  g = [1/z 1]*horner(G,z^2);			// compute synthesis filter
						// from polyphase matrix
  plot_avg_polyphase(g,styles(j));
end

// compute synthesis bank for L=infinity and plot average polyphase response
[R0c,R0ac] = parapinv(H);			// compute causal and anti-causal part
						// of the para-pseudoinverse R0 (opt.
						// inverse of H for L=infinity)
R0 = ss2tf(R0c) + horner(ss2tf(R0ac),1/z);	// transfer function of R0
printf("\n||G|| for delay L=infinity: %.4f\n",sqrt(dh2norm(R0c)^2+dh2norm(R0ac)^2));
g = [1/z 1]*horner(R0,z^2);			// compute synthesis filters
						// from polyphase matrix
plot_avg_polyphase(g,'-k');

// annotate plots
xtitle("","Frequency f []","");
a = get("current_axes");
a.font_style = 2;
a.font_size = 4;
a.title.font_style = 2;
a.title.font_size = 4;
a.x_label.font_style = 2;
a.x_label.font_size = 4;
a.y_label.font_style = 2;
a.y_label.font_size = 4;